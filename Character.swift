//
//  Character.swift
//  BeatEm 2.0
//
//  Created by Rafal Gorczynski on 05.12.2016.
//  Copyright © 2016 Rafal Gorczynski. All rights reserved.
//

import Foundation

class Character {
    var hp: Int
    var mp: Int
    var attack: Int
    var magicAttack: Int
    var vocation: String
    var name: String
    var experience: Int
    var level: Int
    
    init (hp: Int, mp: Int, attack: Int, magicAttack: Int, vocation: String, name: String, experience: Int, level: Int){
        self.hp = hp
        self.mp = mp
        self.attack = attack
        self.magicAttack = magicAttack
        self.vocation = vocation
        self.name = name
        self.experience = experience
        self.level = level
    }
    
    func levelUp() {
        
        level = level + 1
        
        if vocation == "Wizard"{
            hp = hp + 60
            mp = mp + 100
            attack = attack + 20
            magicAttack = magicAttack + 50
        }
        
        if vocation == "Warrior"{
            hp = hp + 100
            mp = mp + 60
            attack = attack + 50
            magicAttack = magicAttack + 20
        }
    }
    
    func checkExperience() {
        if experience >= level * 120 {
            levelUp()
            experience = 0
        }
    }
    
}

var wizard = Character(hp: 200, mp: 400, attack: 20, magicAttack: 40, vocation: "Wizard", name: "Roman", experience: 0, level: 1)

